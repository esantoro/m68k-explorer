FROM ubuntu:14.04
EXPOSE 10240

RUN apt-get update
# RUN apt-get dist-upgrade -y
RUN apt-get install -y nodejs npm
RUN apt-get install -y build-essential
RUN apt-get clean

COPY ./m68k-unknown-elf/ /opt/m68k-unknown-elf/
COPY ./mips-unknown-elf/ /opt/mips-unknown-elf/
COPY ./gcc-explorer/ /opt/gcc-explorer

RUN  cd /opt/gcc-explorer && npm install

ENV PATH /opt/m68k-unknown-elf/bin:/opt/mips-unknown-elf/bin:$PATH

WORKDIR /opt/gcc-explorer

CMD make
