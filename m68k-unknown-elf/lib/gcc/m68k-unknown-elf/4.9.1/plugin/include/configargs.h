/* Generated automatically. */
static const char configuration_arguments[] = "/var/home/manu/gcc-m68k/gcc-m68k/.build/src/gcc-4.9.1/configure --build=x86_64-build_unknown-linux-gnu --host=x86_64-build_unknown-linux-gnu --target=m68k-unknown-elf --prefix=/home/manu/x-tools/m68k-unknown-elf --with-local-prefix=/home/manu/x-tools/m68k-unknown-elf/m68k-unknown-elf/sysroot --disable-libmudflap --with-sysroot=/home/manu/x-tools/m68k-unknown-elf/m68k-unknown-elf/sysroot --with-newlib --enable-threads=no --disable-shared --with-pkgversion='crosstool-NG 1.20.0' --with-cpu=cpu32 --disable-__cxa_atexit --with-gmp=/var/home/manu/gcc-m68k/gcc-m68k/.build/m68k-unknown-elf/buildtools --with-mpfr=/var/home/manu/gcc-m68k/gcc-m68k/.build/m68k-unknown-elf/buildtools --with-mpc=/var/home/manu/gcc-m68k/gcc-m68k/.build/m68k-unknown-elf/buildtools --with-ppl=no --with-isl=no --with-cloog=no --with-libelf=no --disable-lto --with-host-libstdcxx='-static-libgcc -Wl,-Bstatic,-lstdc++,-Bdynamic -lm' --enable-target-optspace --disable-libgomp --disable-libmudflap --disable-nls --disable-multilib --enable-languages=c";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "mcpu=cpu32" } };
